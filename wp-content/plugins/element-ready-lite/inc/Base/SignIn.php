<?php 
namespace Element_Ready\Base;
use Element_Ready\Base\BaseController;

/**
* signIn form widget
*/
class SignIn extends BaseController
{

    
	public function register() {
	
        add_action('init',[$this,'form_submit']);
    }

    function form_validate($data){

        unset( $_SESSION["element_ready_quomodo_login_msg"] );
        if(isset($data['password']) && $data['password'] == ''){
            $_SESSION["element_ready_quomodo_login_msg"]['valid_email'] = esc_html__('Password is not empty','element-ready');
        } 

        if(isset($_SESSION["element_ready_quomodo_login_msg"])){
            return true;
        }    

        return false;
        
    }
    public function login($data){
       
        $creds = array(
            'user_login'    => sanitize_text_field($data['username']),
            'user_password' => sanitize_text_field($data['password']),
            'remember'      => true
        );
       
        $creds['remember'] = sanitize_text_field(isset( $_POST['rememberme'] )?true:false);
        $user = wp_signon( $creds, false );
     
        if ( is_wp_error( $user ) ) {

            $_SESSION["element_ready_quomodo_login_msg"]['valid_email'] = $user->get_error_message();
        
        }else{
        
            $_SESSION["element_ready_quomodo_login_success_msg"]  = esc_html__('Login Success','element-ready');  
        
        }      
    }

    public function form_submit(){
     
        $retrieved_nonce = isset($_REQUEST['_wpnonce'])?$_REQUEST['_wpnonce']:'';

        if ( !wp_verify_nonce($retrieved_nonce, 'element_ready_quomodo_login_action' ) ){
          return;  
        }
        session_start();
        $error =  $this->form_validate($_REQUEST); 
        if($error == false){
          $this->login($_REQUEST);   
        }  
        $request = $_SERVER["HTTP_REFERER"];
        wp_redirect($request); exit;

    }
	

   
}