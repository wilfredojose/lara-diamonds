=== Quick View for WooCommerce ===
Contributors: shapedplugin, rubel_miah, shamimmiashuhagh, khalilu
Tags: woocommerce quick view, woocommerce lightbox, best woocommerce quick view, product quick view, product popup, quick view, free quick view, woo quick view, quickview, product lightbox, product modal, woocommerce, woocommerce extension
Requires at least: 4.8
Tested up to: 5.7.1
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Quick View for WooCommerce allows your customers to quickly view product information in nice Popup without opening the product page. Products can easily be added to cart from popup.

== Description ==

**Quick View for WooCommerce** allows your customers to quickly view product information in nice Popup without opening the product page. Products can easily be added to cart from popup.

[__Live Demo__](https://demo.shapedplugin.com/woocommerce-quick-view/)


= Why Quick View for WooCommerce? =

Imagine you have customers who need to quickly view your products in the shop. How bad experience would it be if every customer has to open them in a new tab or window? Your customers will be free to look in a better way your products, reading also a small description with all the most important features. With the product quick view button, they will be more focused on what they really want and they will proceed easily to the purchase step.


= Key Features =

* Lightweight and powerful.
* **Quick View** button position. (Before and after Add to Cart button)
* Set a quick view button color.
* Change Quick View button text.
* Set popup overlay background color.
* Popup effect.
* Show/hide popup close button.
* Popup close button icon color & hover color.
* Popup close button icon size.
* Set product star rating color.
* Add to cart button background, hover color & custom padding.
* Popup box area background.
* Custom CSS field.
* Translation Ready.
* Compatible with any theme & WooCommerce plugins.
* SEO friendly & optimized for speed.
* Support all modern browsers.
* And much more options.


= FOR FAST SUPPORT, FEATURE REQUEST, AND BUG REPORTING =
> Ask us at [__Support.__](https://shapedplugin.com/support/?user=lite)


= Author =
Designed and Developed by [__ShapedPlugin__](https://shapedplugin.com/)




== Installation ==

**This section describes how to install the plugin and get it working**

= AUTOMATIC INSTALLATION (EASIEST WAY) =

To do an automatic install of Quick View for WooCommerce
, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.
In the search field type "Quick View for WooCommerce
" by ShapedPlugin. Once you have found it you can install it by simply clicking "Install Now" and then "Activate".

= MANUAL INSTALLATION =

**Uploading in WordPress Dashboard**

* Download woo-quickview.zip
* Navigate to the ‘Add New’ in the plugins dashboard
* Navigate to the ‘Upload’ area
* Select woo-quickview.zip from your computer
* Click ‘Install Now’
* Activate the plugin in the Plugin dashboard

**Using FTP**

* Download woo-quickview.zip
* Extract the woo-quickview directory to your computer
* Upload the woo-quickview directory to the /wp-content/plugins/ directory
* Activate the plugin in the Plugin dashboard

The WordPress codex contains <a href="https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation">instructions</a> on how to install a WordPress plugin.


== Frequently Asked Questions ==


== Screenshots ==
1. Live Demo
2. Button Settings
3. Popup Settings
4. Custom CSS Field


== Changelog ==

= 1.0.9 - Apr 15, 2021 =
* Tested: WordPress 5.7.1 compatibility.
* Tested: WooCommerce 5.2.1 compatibility.

= 1.0.8 - Mar 16, 2021 =
* Tested: WordPress 5.7 compatibility.
* Tested: WooCommerce 5.1.0 compatibility.

= 1.0.7 - Dec 12, 2020 =
* Fix: WordPress 5.6 compatibility issue.
* Fix: WooCommerce 4.8.0 compatibility issue.

= 1.0.6 - Aug 19, 2020 =
* Fix: WordPress 5.5 compatibility issue.

= 1.0.5 - Apr 09, 2020 =
* Fix: WordPress 5.4 compatibility issue.
* Fix: WooCommerce 4.0.1 compatibility issue.

= 1.0.4 - Nov 26, 2019 =
* Fix: WordPress 5.3 compatibility issue.

= 1.0.3 - Apr 25, 2019 =
* Fix: Conflict on infinite scroll pagination.

= 1.0.2 - Apr 09, 2019 =
* New: Languages .pot file.
* Fix: CSS conflicting issue.

= 1.0.1 - Apr 05, 2019 =
* New: Quick View button border option.

= 1.0 - Feb 02, 2019 =
* First Release