<?php

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Manages externa compatibility functions folder
 *
 *
 * @version		1.0.0
 * @package		ecommerce-product-catalog/ext-comp
 * @author 		impleCode
 */
class ic_catalog_builders_compat {

	function __construct() {
		add_action( 'ic_catalog_wp', array( $this, 'wp' ) );
		add_filter( 'et_builder_post_types', array( $this, 'divi_builder_enable' ) );
		add_filter( 'ic_shortcode_catalog_apply', array( $this, 'disable_shortcode_catalog' ) );
	}

	function divi_builder_enable( $post_types ) {
		$post_types[] = 'al_product';
		return $post_types;
	}

	function wp() {
		remove_action( 'wp_enqueue_scripts', 'et_divi_replace_stylesheet', 99999998 );
	}

	function disable_shortcode_catalog( $disable ) {
		if ( function_exists( 'et_theme_builder_get_template_layouts' ) ) {
			$layouts = et_theme_builder_get_template_layouts();
			if ( !empty( $layouts[ 'et_body_layout' ][ 'enabled' ] ) && !empty( $layouts[ 'et_body_layout' ][ 'override' ] ) ) {
				$disable = true;
			}
		}
		return $disable;
	}

}

$ic_catalog_builders_compat = new ic_catalog_builders_compat;
