=== Ferry ===
Contributors: Themeansar
Author: Themeansar
Requires at least: WordPress 4.7
Tested up to: WordPress 5.4
Requires PHP: 5.6
Stable tag: 2.6.3
Version: 2.6.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns ,right-sidebar, flexible-header, custom-background, custom-header, custom-menu, editor-style, featured-images, footer-widgets,  theme-options, threaded-comments, rtl-language-support, translation-ready, full-width-template, custom-logo, blog, news

== Description ==
ferry is a powerful bootstrap WordPress theme for ecommerce and woocommerce. This theme designed for ferry (e-commerce business , online-shop, store, online business etc.). This theme is packed with lots of exciting feature that enhances the ecommerce experience. This theme package many premium features and several custom widgets which helps making your online store.Theme suitable for photography, e-shop, gallary, magzine, blog, blog full-width, page full-width, blog masonry, blog slider, service, fashion, portfolio, music. View the demo of ferry Premium https://themeansar.com/demo/wp/ferry/default/

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in ferry in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.

== Copyright ==

Ferry WordPress Theme, Copyright (c) 2020, Themeansar
Ferry is distributed under the terms of the GNU GPLs

== Credits ==


/***** BUNDELED CSS ***/
============================================
This theme uses Underscores
============================================
 * ferry is based on Underscores. All the files in the theme package are from Underscores, unless stated otherwise.
 * Copyright: Automattic, automattic.com
 * Source: http://underscores.me/
 * License: GPLv2
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html

============================================
This theme uses Font Awesome for the theme icons
============================================
 * Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
 * Source: http://fontawesome.io
 * License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
======================================
This theme uses Bootstrap as a design tool
======================================
 * Bootstrap (http://getbootstrap.com/)
 * Copyright (c) 2011-2014 Twitter, Inc
 * Licensed under https://github.com/twbs/bootstrap/blob/master/LICENSE
 
======================================

This theme uses daneden/animate
======================================
 * Animate  : https://github.com/daneden
 * Copyright (c) 2016 Daniel Eden ( https://github.com/daneden/animate.css/blob/master/LICENSE )
 * Licensed under https://github.com/daneden/animate.css
======================================

/***** BUNDELED JS ***/
This theme uses Owl Carousel
======================================

 * Owner : https://github.com/OwlFonk
 * The MIT License (MIT) Copyright (c) 2013 (https://github.com/OwlFonk/OwlCarousel/blob/master/LICENSE)
 * Licensed under https://github.com/OwlFonk/OwlCarousel
======================================

This theme uses vadikom/smartmenus
======================================

 * Owner : https://github.com/vadikom
 * Copyright (c) Vasil Dinkov, Vadikom Web Ltd. ( https://github.com/vadikom/smartmenus/blob/master/LICENSE-MIT)
 * Licensed under https://github.com/vadikom/smartmenus
======================================

This theme uses sticky/menu
======================================
 * Owner : https://github.com/garand
 * Copyright 2014-2016 Anthony Garand http://garand.me ( https://github.com/garand/sticky/blob/master/LICENSE.md)
 * Licensed under https://github.com/garand/sticky
======================================

This theme uses waypoints
======================================
 * Owner : https://github.com/imakewebthings
 * Copyright (c) 2011-2012 Caleb Troughton (https://github.com/imakewebthings/waypoints/blob/master/licenses.txt)
 * Licensed under https://github.com/imakewebthings/waypoints
======================================

WP Bootstrap navwalker
======================================
URI: https://github.com/twittem/wp-bootstrap-navwalker
Version: 2.0.4
Author: Edward McIntyre - @twittem 
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt


/**** Images Source *****/
== Post Image ==
Image for theme screenshot, Copyright The Lazy Artist Gallery
License: Creative Commons CC0 license.
License URI: https://stocksnap.io/license
Source: https://stocksnap.io/photo/woman-blue-0FMO7ARXDL

== Breadcrumb Image ==
Image for theme screenshot, Copyright Edu Lauton
License: Creative Commons CC0 license.
License URI: https://stocksnap.io/license
Source: https://stocksnap.io/photo/yellow-wood-UGKIE5LWCQ

--- Version 0.1 ----
1. Relesed

--- Version 0.2 ----
1. Update Prefix issue.

2. Update minfied and unminiied version.

--- Version 0.3 ----	
1. Update prefix and transaltion issue.	

--- Version 0.4 ----	
1. Update prefix and ads widget.

--- Version 0.5 ----	
1. Update prefix and styling issue.

--- Version 0.6 ----
1. Solved Theme Review issues.
	
--- Version 0.7 ----
1. Solved Theme Review issue.

--- Version 0.8 ----
1. Solved Theme Review issue.

--- Version 0.9 ----
1. Fixed Theme Review issue.

--- Version 1.0 ----
1. Fixed Header Image issue.

--- Version 1.1 ----
1. Fixed site title & site description color issue.
2. fixed nav-menu issue.

--- Version 1.2 ----
1. Add Ads Section in homepage.
2. Change Screenshot image.

--- Version 1.3 ----
1. Update font-awesome.

--- Version 1.4 ----
1. remove unused woocommerce code.

--- Version 1.5 ----
1. Remove comment.php unused code.
2. Update strings.

--- Version 1.6 ----
1. Remove unused code in customizer.
2. Remove unused images.

--- Version 1.7 ----
1. Remove unused css.

--- Version 1.8 ----
1. Fixed gallery styling issue.

--- Version 1.9 ----
1. Fixed slider sticky post issue.

--- Version 2.0 ----
1. Update Header Section.

--- Version 2.1 ----
1. Update 404 link.

--- Version 2.2 ----
1. Fixed woocommerce domain argument.

--- Version 2.3 ----
1. Added woocommerce activation page.

--- Version 2.4 ----
1. Fixed woocommerce recent product issue.

--- Version 2.5 ----
1. Added woocommerce activation page.

--- Version 2.5.1 ----
1. Change customize section priority.

--- Version 2.5.2 ----
1. Fixed social icon issue.

--- Version 2.5.4 ----
1. Added theme info detail page.

--- Version 2.5.5 ----
1. Update Theme Demo URL.
2. Fixed meta issue in blog section.

--- Version 2.5.6 ----
1. Fixed feature product issue.

--- Version 2.5.7 ----
1. Fixed feature product styling issue.

--- Version 2.5.8 ----
1. Fixed styling issue.

--- Version 2.5.9 ----
1. Fix the navigation styling issue.

--- Version 2.6 ----
1. Fix the section issue.

--- Version 2.6.1 ----
1. Fixed the styling issue.

--- Version 2.6.2 ----
1. Fixed ads section issue.

--- Version 2.6.3 ----
1. Added skip links.

--- Version 2.6.4 ----
1. Fixed styling issue.
2. Update screenshot update.
3. bootstrap 4 update.