<?php
define( 'WP_CACHE', true );
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lara_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$|4p],1~0&S-@{aS3:C(`,Nk(m;Dn~}{vBm{ZR-ud+_JY6:7tS6VCJ[-dEWWQ+28' );
define( 'SECURE_AUTH_KEY',  'F7bcP0/luwZ6{hjPfPw|tp=<;T jxTUN7raVRF@%ew2W]:[%2f*7MnFi*Avw,#6X' );
define( 'LOGGED_IN_KEY',    '}rH~ujXH0p~LagHp/cHsZ!cRwwANp:cn&C)][o~8FiJy3W)2z0AKVXet|xp4$K+$' );
define( 'NONCE_KEY',        'xv`eZ$z^454xq{XLO*qo]Tk|[>f1{{c-h&2vg,.7vrNGnPPg>oKDZnesZ$]yJ9~Y' );
define( 'AUTH_SALT',        'fCy;#i&A_gg{y>_U5EmLu;%fNji*q2gI`J!y|GkJXzkdE-?Kp#!&[UBV!D D6E-Z' );
define( 'SECURE_AUTH_SALT', '5$4LKaR;LEE2Y=o0cf3V[9NcgN,Z9}{Ow>>/)HRS1G <yNVm8=o.%:/B$j?iJxk}' );
define( 'LOGGED_IN_SALT',   '[p/i+>S>bFR,ii`{{wO>>ex$PM7yR9G NVX T|DybhJVaA/,MvqMc%-OS$=xAo[F' );
define( 'NONCE_SALT',       '#W:J8ULY_N57Ym8{V/zkN)W#V;rm0II#Doh0,mA#t{6J+qUjqw@AXj|i+MJqiB4_' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
